# minimum system discover iot

An example project for combining an ADC, ds18b20 temp sensor, buttons and leds in a minimum system that reads the peripherals and sends values to thingspeak.  
The minimum system is a part of a course at UCL called "Discover IoT", For more info visit the [companion site](https://eal-itt.gitlab.io/discover-iot/) and the [course material](https://eal-itt.gitlab.io/20a-itt1-project)

# Development usage

You need a RPi(>=3), DS18B20 temperature sensor, MCP3008 ADC, Potentiometer, led/btn board, Python above version 3.5, Thingspeak account, pip installed and SPI enabled on the Raspberry Pi 

1. Clone the repository `git clone git@gitlab.com:npes-py-experiments/minimum-system-discover-iot.git` 
2. Create a virtual environment https://packaging.python.org/guides/installing-using-pip-and-virtual-environments/#creating-a-virtual-environment 
2. Activate the virtual environment https://packaging.python.org/guides/installing-using-pip-and-virtual-environments/#activating-a-virtual-environment
3. Install requirements `pip install -r requirements.txt`
4. On the Raspberry Pi, you will need to add dtoverlay=w1-gpio" (for regular connection) or dtoverlay=w1-gpio,pullup="y" (for parasitic connection) to your /boot/config.txt. The default data pin is GPIO4 (RaspPi connector pin 7), but that can be changed from 4 to x with dtoverlay=w1-gpio,gpiopin=x 
5. Run `python3 app.py`