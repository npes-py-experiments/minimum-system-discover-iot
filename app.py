from gpiozero import Button, MCP3008
import time
import thingspeak
from w1thermsensor import W1ThermSensor  # library for the sensor https://pypi.org/project/w1thermsensor/
from functions import *
import json
import pprint

# initialize variables
btn1 = Button(21)
btn2 = Button(20)
temp_sensor = W1ThermSensor()
adc = MCP3008(channel=0, clock_pin=11, mosi_pin=10, miso_pin=9, select_pin=8)
ts_loop_duration = 15000  # interval sending to thingspeak in milliseconds
temp_loop_duration = 2000  # interval reading temp_sensor in milliseconds
ts_loop_start = now_ms()
temp_loop_start = now_ms()
channel_id = get_key('channel.txt')   # from channel.txt in same folder as app.py
write_key = get_key('write_key.txt')  # # from write_key.txt in same folder as app.py
channel = thingspeak.Channel(id=channel_id, api_key=write_key)
btn1_counter = 0
btn2_counter = 0
temp_sum = 0.0
times = 0
adc_value = 0.0

while True:
    if now_ms() - ts_loop_start > ts_loop_duration:
        # send to thingspeak
        response = channel.update({1: btn1_counter, 2: btn2_counter, 3: calc_avg(temp_sum, times), 4: adc_value})
        update_led2()
        pprint.pprint(json.loads(response))
        # reset variables
        btn1_counter = 0
        btn2_counter = 0
        temp_sum = 0.0
        times = 0
        ts_loop_start = now_ms()
        continue
    elif now_ms() - temp_loop_start > temp_loop_duration:
        temp_sum += temp_sensor.get_temperature()
        times += 1
        temp_loop_start = now_ms()
    else:
        adc_value = raw_to_voltage(adc.raw_value)
        if btn1.is_pressed:
            time.sleep(0.2)  # debounce button by waiting 0.2 secs
            btn1_counter += 1
        elif btn2.is_pressed:
            time.sleep(0.2)  # debounce button by waiting 0.2 secs
            btn2_counter += 1
        elif adc_value > 2.00:
            update_led1()
