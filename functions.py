import sys
import time
from gpiozero import LED

led1 = LED(16)
led2 = LED(12)

# open and format write key
def get_key(filename, out=sys.stdout):
    out.write('using key path {}\n'.format(filename))
    with open(filename) as f:
        key = f.readline()
    out.write("- read key with {} chars\n".format(len(key.strip())))
    return key.strip()

def now_ms():
    ms = int(time.time() * 1000)
    return ms

def calc_avg(value, times):
    average = value/times
    return average

def update_led1():
    for i in range(3):
        led1.on()
        time.sleep(0.2)
        led1.off()
        time.sleep(0.2)

def update_led2():
        led2.on()
        time.sleep(0.5)
        led2.off()

def raw_to_voltage(raw_val):
    result = raw_val*(3.3/1024)
    return result
